# [1.5.0](https://gitlab.com/deptest1/testlib/compare/v1.4.0...v1.5.0) (2021-05-03)


### New

* added function hi() ([4b925a0](https://gitlab.com/deptest1/testlib/commit/4b925a04d3f6e49e87e772914b9010dee5d2d3fb))

# [1.4.0](https://gitlab.com/deptest1/testlib/compare/v1.3.2...v1.4.0) (2021-05-01)


### Chore

* add changelog ([832d51a](https://gitlab.com/deptest1/testlib/commit/832d51a981a84bd6d4736c6e055f184b76fd10d0))
* remove downstream trigger for not main branches ([c4d9f9a](https://gitlab.com/deptest1/testlib/commit/c4d9f9a685b12cfa11c81e535128d04fa2f8829d))
* try changelog based on eslint config ([94e9719](https://gitlab.com/deptest1/testlib/commit/94e971974b287a32ba62ab9cd80530e1dc1254df))
* try changelog based on eslint config ([b4b4a1b](https://gitlab.com/deptest1/testlib/commit/b4b4a1b595fb568403222cb07b50c79a2b083552))

### New

* added function hi() ([e6fed26](https://gitlab.com/deptest1/testlib/commit/e6fed262cbd7fd27f44ca17cb4cf63d3f3d1c9a1))

### Upgrade

* upgrade dependencies ([6faaebd](https://gitlab.com/deptest1/testlib/commit/6faaebdadb5c901fb4b1bef05a6857c4fa82ae5d))
* upgrade dependencies2 ([bc13bf8](https://gitlab.com/deptest1/testlib/commit/bc13bf8358b8308e12b3ad83843aed6dcc855f28))
* upgrade dependencies3 ([b0d1fce](https://gitlab.com/deptest1/testlib/commit/b0d1fceaa759fc03a39a5e8580724549bb15a7af))
