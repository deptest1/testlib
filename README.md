# testlib

This is a library used by the testproject.

Simple Typescript project using yarn2.

## Installation

```bash
# install dependencies
$ yarn

# build
$ yarn build

# run tests incl. test coverage
$ yarn test

# update vscode config (requires VSCode restart)
yarn dlx @yarnpkg/pnpify --sdk vscode

# run prettier (format all files), prettier runs automatically before commit
yarn prettier --write .

# run eslint (find problems and fix the obvious)
yarn eslint --fix src

```

## Usage of the "library"

```typescript
import { hello } from "@deptest/testlib";

hello("World");
// results in 'Hello World!' on console
```

## Commit Message Format

```
<type>(<scope>): <short summary>
  │       │             │
  │       │             └─⫸ Summary in present tense. Not capitalized. No period at the end.
  │       │
  │       └─⫸ Commit Scope: animations|bazel|benchpress|common|compiler|compiler-cli|core|
  │                          elements|forms|http|language-service|localize|platform-browser|
  │                          platform-browser-dynamic|platform-server|router|service-worker|
  │                          upgrade|zone.js|packaging|changelog|dev-infra|docs-infra|migrations|
  │                          ngcc|ve
  │
  └─⫸ Commit Type: build|ci|docs|feat|fix|perf|refactor|test
```

The `<type>` and `<summary>` fields are mandatory, the `(<scope>)` field is optional.

##### Type

Must be one of the following:

- **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- **ci**: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
- **docs**: Documentation only changes
- **feat**: A new feature
- **fix**: A bug fix
- **perf**: A code change that improves performance
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **test**: Adding missing tests or correcting existing tests

##### Scope

The scope should be the name of the npm package affected (as perceived by the person reading the changelog generated from commit messages).

The following is the list of supported scopes:

- `animations`
- `bazel`
- `benchpress`
- `common`
- `compiler`
- `compiler-cli`
- `core`
- `elements`
- `forms`
- `http`
- `language-service`
- `localize`
- `platform-browser`
- `platform-browser-dynamic`
- `platform-server`
- `router`
- `service-worker`
- `upgrade`
- `zone.js`

There are currently a few exceptions to the "use package name" rule:

- `packaging`: used for changes that change the npm package layout in all of our packages, e.g. public path changes, package.json changes done to all packages, d.ts file/format changes, changes to bundles, etc.

- `changelog`: used for updating the release notes in CHANGELOG.md

- `dev-infra`: used for dev-infra related changes within the directories /scripts, /tools and /dev-infra

- `docs-infra`: used for docs-app (angular.io) related changes within the /aio directory of the repo

- `migrations`: used for changes to the `ng update` migrations.

- `ngcc`: used for changes to the [Angular Compatibility Compiler](./packages/compiler-cli/ngcc/README.md)

- `ve`: used for changes specific to ViewEngine (legacy compiler/renderer).

- none/empty string: useful for `test` and `refactor` changes that are done across all packages (e.g. `test: add missing unit tests`) and for docs changes that are not related to a specific package (e.g. `docs: fix typo in tutorial`).

##### Summary

Use the summary field to provide a succinct description of the change:

- use the imperative, present tense: "change" not "changed" nor "changes"
- don't capitalize the first letter
- no dot (.) at the end

Tets
