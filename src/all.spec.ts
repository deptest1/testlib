import { hello, hi } from "./index";

describe("Test", () => {
  test("test text output", async () => {
    expect(hello("World")).toEqual("Hello World!");
  });
});

describe("Test", () => {
  test("test text output", async () => {
    expect(hi("World")).toEqual("Hi World!");
  });
});
