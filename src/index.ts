export function hello(name: string): string {
  return `Hello ${name}!`;
}

export function hi(yourname: string): string {
  return `Hi ${yourname}!`;
}
